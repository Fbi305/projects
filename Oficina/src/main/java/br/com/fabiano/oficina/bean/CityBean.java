package br.com.fabiano.oficina.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.fabiano.oficina.dao.CityDAO;
import br.com.fabiano.oficina.dao.StateDAO;
import br.com.fabiano.oficina.domain.City;
import br.com.fabiano.oficina.domain.State;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class CityBean implements Serializable {
	private City city;
	private List<City> cities;
	private List<State> states;

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public void news() {
		try {
			city = new City();

			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar gerar a cidade");
			error.printStackTrace();
		}
	}

	@PostConstruct
	public void list() {
		try {
			CityDAO cityDAO = new CityDAO();
			cities = cityDAO.list();

		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar listar as cidades");
			error.printStackTrace();
		}
	}

	public void save() {
		try {
			CityDAO cityDAO = new CityDAO();
			cityDAO.merge(city);

			news();

			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();

			cities = cityDAO.list();
			Messages.addGlobalInfo("Salvo com sucesso!");
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar salvar a cidade");
			error.printStackTrace();
		}
	}

	public void edit(ActionEvent event) {
		try {
			city = (City) event.getComponent().getAttributes().get("selectedCity");

			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar editar a cidade");
			error.printStackTrace();
		}
	}

	public void delete(ActionEvent event) {
		try {
			city = (City) event.getComponent().getAttributes().get("selectedCity");

			CityDAO cityDAO = new CityDAO();
			cityDAO.delete(city);

			cities = cityDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar excluir a cidade");
			error.printStackTrace();
		}
	}
}