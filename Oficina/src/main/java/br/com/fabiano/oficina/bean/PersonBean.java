package br.com.fabiano.oficina.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.fabiano.oficina.dao.CityDAO;
import br.com.fabiano.oficina.dao.PersonDAO;
import br.com.fabiano.oficina.dao.StateDAO;
import br.com.fabiano.oficina.domain.City;
import br.com.fabiano.oficina.domain.Person;
import br.com.fabiano.oficina.domain.State;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class PersonBean implements Serializable {
	private Person person;
	private List<Person> persons;
	private List<State> states;
	private List<City> cities;

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public void news() {
		try {
			person = new Person();

			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();

			CityDAO cityDAO = new CityDAO();
			cities = cityDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar gerar a pessoa");
			error.printStackTrace();
		}
	}

	@PostConstruct
	public void list() {
		try {
			PersonDAO personDAO = new PersonDAO();
			persons = personDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar listar as pessoas");
			error.printStackTrace();
		}
	}

	public void save() {
		try {
			PersonDAO personDAO = new PersonDAO();
			personDAO.merge(person);

			news();

			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();

			CityDAO cityDAO = new CityDAO();
			cities = cityDAO.list();

			persons = personDAO.list();
			Messages.addGlobalInfo("Salvo com sucesso!");
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar salvar a pessoa");
			error.printStackTrace();
		}
	}

	public void edit(ActionEvent event) {
		try {
			person = (Person) event.getComponent().getAttributes().get("selectedPerson");

			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();

			CityDAO cityDAO = new CityDAO();
			cities = cityDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar editar a pessoa");
			error.printStackTrace();
		}
	}

	public void delete(ActionEvent event) {
		try {
			person = (Person) event.getComponent().getAttributes().get("selectedPerson");

			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();

			CityDAO cityDAO = new CityDAO();
			cities = cityDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar excluir a pessoa");
			error.printStackTrace();
		}
	}
}