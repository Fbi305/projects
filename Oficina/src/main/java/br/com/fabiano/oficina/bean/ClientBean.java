package br.com.fabiano.oficina.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.fabiano.oficina.dao.ClientDAO;
import br.com.fabiano.oficina.dao.PersonDAO;
import br.com.fabiano.oficina.domain.Client;
import br.com.fabiano.oficina.domain.Person;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class ClientBean implements Serializable {
	private Client client;
	private List<Client> clients;
	private List<Person> persons;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public void news() {
		try {
			client = new Client();

			PersonDAO personDAO = new PersonDAO();
			persons = personDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar gerar o cliente");
			error.printStackTrace();
		}
	}

	@PostConstruct
	public void list() {
		try {
			ClientDAO clientDAO = new ClientDAO();
			clients = clientDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar listar os clientes");
			error.printStackTrace();
		}
	}

	public void save() {
		try {
			ClientDAO clientDAO = new ClientDAO();
			clientDAO.merge(client);

			news();

			PersonDAO personDAO = new PersonDAO();
			persons = personDAO.list();

			clients = clientDAO.list();
			Messages.addGlobalInfo("Salvo com sucesso!");
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar salvar o cliente");
			error.printStackTrace();
		}
	}

	public void edit(ActionEvent event) {
		try {
			client = (Client) event.getComponent().getAttributes().get("selectedClient");

			PersonDAO personDAO = new PersonDAO();
			persons = personDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar editar o cliente");
			error.printStackTrace();
		}
	}

	public void delete(ActionEvent event) {
		try {
			client = (Client) event.getComponent().getAttributes().get("selectedClient");

			ClientDAO clientDAO = new ClientDAO();
			clientDAO.delete(client);

			clients = clientDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar excluir o cliente");
			error.printStackTrace();
		}
	}
}