package br.com.fabiano.oficina.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.fabiano.oficina.dao.ManufacturerDAO;
import br.com.fabiano.oficina.domain.Manufacturer;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class ManufacturerBean implements Serializable {
	private Manufacturer manufacturer;
	private List<Manufacturer> manufacturers;

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public List<Manufacturer> getManufacturers() {
		return manufacturers;
	}

	public void setManufacturers(List<Manufacturer> manufacturers) {
		this.manufacturers = manufacturers;
	}

	public void news() {
		manufacturer = new Manufacturer();
	}

	@PostConstruct
	public void list() {
		try {
			ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
			manufacturers = manufacturerDAO.list();

		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar gerar o fabricante");
			error.printStackTrace();
		}
	}

	public void save() {
		try {
			ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
			manufacturerDAO.merge(manufacturer);

			news();

			manufacturers = manufacturerDAO.list();
			Messages.addGlobalInfo("Salvo com sucesso!");
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar salvar o fabricante");
			error.printStackTrace();
		}
	}

	public void edit(ActionEvent event) {
		manufacturer = (Manufacturer) event.getComponent().getAttributes().get("selectedManufacturer");
	}

	public void delete(ActionEvent event) {
		try {
			manufacturer = (Manufacturer) event.getComponent().getAttributes().get("selectedManufacturer");

			ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
			manufacturerDAO.delete(manufacturer);

			manufacturers = manufacturerDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar excluir o fabricante");
			error.printStackTrace();
		}
	}
}