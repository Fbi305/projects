package br.com.fabiano.oficina.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import br.com.fabiano.oficina.util.HibernateUtil;

public class GenericDAO<T> {
	private Class<T> classes;
	private Session session = null;
	private Transaction transaction = null;

	@SuppressWarnings("unchecked")
	public GenericDAO() {
		this.classes = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public void save(T t) {
		session = HibernateUtil.getSessionFactory().openSession();
		try {
			transaction = session.beginTransaction();
			session.save(t);
			transaction.commit();
		} catch (RuntimeException error) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw error;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> list() {
		session = HibernateUtil.getSessionFactory().openSession();
		try {
			Criteria consult = session.createCriteria(classes);
			List<T> result = consult.list();
			return result;
		} catch (Exception error) {
			throw error;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public T search(Long cod) {
		session = HibernateUtil.getSessionFactory().openSession();
		try {
			Criteria consult = session.createCriteria(classes);
			consult.add(Restrictions.idEq(cod));
			T result = (T) consult.uniqueResult();
			return result;
		} catch (Exception error) {
			throw error;
		} finally {
			session.close();
		}
	}

	public void delete(T t) {
		session = HibernateUtil.getSessionFactory().openSession();
		try {
			transaction = session.beginTransaction();
			session.delete(t);
			transaction.commit();
		} catch (Exception error) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw error;
		} finally {
			session.close();
		}
	}

	public void edit(T t) {
		session = HibernateUtil.getSessionFactory().openSession();
		try {
			transaction = session.beginTransaction();
			session.update(t);
			transaction.commit();
		} catch (Exception error) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw error;
		} finally {
			session.close();
		}
	}

	public void merge(T t) {
		session = HibernateUtil.getSessionFactory().openSession();
		try {
			transaction = session.beginTransaction();
			session.merge(t);
			transaction.commit();
		} catch (RuntimeException error) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw error;
		} finally {
			session.close();
		}
	}
}