package br.com.fabiano.oficina.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.fabiano.oficina.dao.StateDAO;
import br.com.fabiano.oficina.domain.State;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class StateBean implements Serializable {
	private State state;
	private List<State> states;

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public void news() {
		state = new State();
	}

	@PostConstruct
	public void list() {
		try {
			StateDAO stateDAO = new StateDAO();
			states = stateDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar gerar o estado");
			error.printStackTrace();
		}
	}

	public void save() {
		try {
			StateDAO stateDAO = new StateDAO();
			stateDAO.merge(state);

			news();

			states = stateDAO.list();
			Messages.addGlobalInfo("Salvo com sucesso!");
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar salvar o estado");
			error.printStackTrace();
		}
	}

	public void edit(ActionEvent event) {
		state = (State) event.getComponent().getAttributes().get("selectedState");
	}

	public void delete(ActionEvent event) {
		try {
			state = (State) event.getComponent().getAttributes().get("selectedState");

			StateDAO stateDAO = new StateDAO();
			stateDAO.delete(state);

			states = stateDAO.list();
			Messages.addGlobalInfo("Removido com sucesso!");
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar excluir o estado");
			error.printStackTrace();
		}
	}
}