package br.com.fabiano.oficina.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.fabiano.oficina.dao.ManufacturerDAO;
import br.com.fabiano.oficina.dao.ProductDAO;
import br.com.fabiano.oficina.domain.Manufacturer;
import br.com.fabiano.oficina.domain.Product;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class ProductBean implements Serializable {
	private Product product;
	private List<Product> products;
	private List<Manufacturer> manufactures;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<Manufacturer> getManufactures() {
		return manufactures;
	}

	public void setManufactures(List<Manufacturer> manufactures) {
		this.manufactures = manufactures;
	}

	public void news() {
		try {
			product = new Product();

			ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
			manufactures = manufacturerDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar gerar o produto");
			error.printStackTrace();
		}
	}

	@PostConstruct
	public void list() {
		try {
			ProductDAO productDAO = new ProductDAO();
			products = productDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar listar os produtos");
			error.printStackTrace();
		}
	}

	public void save() {
		try {
			ProductDAO productDAO = new ProductDAO();
			productDAO.merge(product);

			news();

			ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
			manufactures = manufacturerDAO.list();

			products = productDAO.list();
			Messages.addGlobalInfo("Salvo com sucesso!");
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar salvar o produto");
			error.printStackTrace();
		}
	}

	public void edit(ActionEvent event) {
		try {
			product = (Product) event.getComponent().getAttributes().get("selectedProduct");

			ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
			manufactures = manufacturerDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar editar o produto");
			error.printStackTrace();
		}
	}

	public void delete(ActionEvent event) {
		try {
			product = (Product) event.getComponent().getAttributes().get("selectedProduct");

			ProductDAO productDAO = new ProductDAO();
			productDAO.delete(product);

			products = productDAO.list();
		} catch (RuntimeException error) {
			Messages.addGlobalError("Erro ao tentar editar o produto");
			error.printStackTrace();
		}
	}
}