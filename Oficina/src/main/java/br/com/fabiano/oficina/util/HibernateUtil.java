package br.com.fabiano.oficina.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	
	private static SessionFactory sesionFactory = buildSessionFactory();

	public static SessionFactory getSessionFactory() {
		return sesionFactory;
	}

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration config = new Configuration().configure();

			ServiceRegistry register = new StandardServiceRegistryBuilder().applySettings(config.getProperties())
					.build();
			SessionFactory session = config.buildSessionFactory(register);
			return session;
		} catch (Throwable ex) {
			System.out.println("Initial SessionFactory creation falied" + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
}