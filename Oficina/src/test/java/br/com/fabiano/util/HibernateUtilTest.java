package br.com.fabiano.util;

import org.hibernate.Session;
import org.junit.Test;

import br.com.fabiano.oficina.util.HibernateUtil;

public class HibernateUtilTest {
	@Test
	public void connect() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.close();
		HibernateUtil.getSessionFactory().close();
	}
}