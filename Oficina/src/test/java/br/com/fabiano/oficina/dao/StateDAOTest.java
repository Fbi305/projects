package br.com.fabiano.oficina.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.State;

public class StateDAOTest {
	@Test
	@Ignore
	public void save() {
		State state = new State();
		state.setName("Rio de Janeiro");
		state.setInitials("RJ");

		StateDAO stateDAO = new StateDAO();
		stateDAO.save(state);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Test
	@Ignore
	public void list() {
		StateDAO stateDao = new StateDAO();
		List<State> result = stateDao.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (State state : result) {
			System.out.println("\nCódigo: " + state.getCod());
			System.out.println("Nome: " + state.getName());
			System.out.println("Sigla: " + state.getInitials());
		}
	}

	@Ignore
	@Test
	public void search() {
		StateDAO stateDAO = new StateDAO();
		State state = stateDAO.search(1L);

		if (state != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCódigo: " + state.getCod());
			System.out.println("Nome: " + state.getName());
			System.out.println("Sigla: " + state.getInitials());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		StateDAO stateDAO = new StateDAO();
		State state = stateDAO.search(3L);

		if (state != null) {
			stateDAO.delete(state);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nCódigo: " + state.getCod());
			System.out.println("Nome: " + state.getName());
			System.out.println("Sigla: " + state.getInitials());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings({ "unused" })
	@Ignore
	@Test
	public void edit() {
		StateDAO stateDAO = new StateDAO();
		State state = stateDAO.search(1L);

		if (state != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCódigo: " + state.getCod());
			System.out.println("Nome: " + state.getName());
			System.out.println("Sigla: " + state.getInitials());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		state.setName("São Paulo");
		state.setInitials("SP");

		if (state != null) {
			stateDAO.edit(state);

			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nCódigo: " + state.getCod());
			System.out.println("Nome: " + state.getName());
			System.out.println("Sigla: " + state.getInitials());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}
}