package br.com.fabiano.oficina.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.Manufacturer;

public class ManufacturerDAOTest {
	@Ignore
	@Test
	public void save() {
		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setDescription("VALEO");

		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		manufacturerDAO.save(manufacturer);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		List<Manufacturer> result = manufacturerDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (Manufacturer manufacturer : result) {
			System.out.println("\nCódigo: " + manufacturer.getCod());
			System.out.println("Descrição: " + manufacturer.getDescription());
		}
	}

	@Ignore
	@Test
	public void search() {
		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		Manufacturer manufacturer = manufacturerDAO.search(1L);

		if (manufacturer != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCódigo: " + manufacturer.getCod());
			System.out.println("\nDescrição: " + manufacturer.getDescription());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		Manufacturer manufacturer = manufacturerDAO.search(6L);

		if (manufacturer != null) {
			manufacturerDAO.delete(manufacturer);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nCódigo: " + manufacturer.getCod());
			System.out.println("Descrição: " + manufacturer.getDescription());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() {
		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		Manufacturer manufacturer = manufacturerDAO.search(7L);

		manufacturer.setDescription("HAHAHA");

		if (manufacturer != null) {
			manufacturerDAO.edit(manufacturer);
			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nCódigo: " + manufacturer.getCod());
			System.out.println("\nDescrição: " + manufacturer.getDescription());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void merge() {
		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setDescription("VALEO");

		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		manufacturerDAO.merge(manufacturer);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}
}