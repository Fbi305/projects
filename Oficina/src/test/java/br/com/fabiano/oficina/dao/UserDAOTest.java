package br.com.fabiano.oficina.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.Person;
import br.com.fabiano.oficina.domain.User;

public class UserDAOTest {

	@Ignore
	@Test
	public void save() {
		PersonDAO personDAO = new PersonDAO();
		Person person = personDAO.search(3L);

		User user = new User();
		user.setActive(true);
		user.setType('A');
		user.setPassword("fp656565");
		user.setPerson(person);

		UserDAO userDAO = new UserDAO();
		userDAO.save(user);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		UserDAO userDAO = new UserDAO();
		List<User> result = userDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (User user : result) {
			System.out.println("\nSituação: " + user.isActive());
			System.out.println("Tipo: " + user.getType());
			System.out.println("Nome: " + user.getPerson().getName());
		}
	}

	@Ignore
	@Test
	public void search() {
		UserDAO userDAO = new UserDAO();
		User user = userDAO.search(1L);

		if (user != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nSituação: " + user.isActive());
			System.out.println("Tipo: " + user.getType());
			System.out.println("Nome: " + user.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		UserDAO userDAO = new UserDAO();
		User user = userDAO.search(4L);

		if (user != null) {
			userDAO.delete(user);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("Situação: " + user.isActive());
			System.out.println("\nTipo: " + user.getType());
			System.out.println("Nome: " + user.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() {
		UserDAO userDAO = new UserDAO();
		User user = userDAO.search(3L);

		if (user != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("Situação: " + user.isActive());
			System.out.println("Tipo: " + user.getType());
			System.out.println("Nome: " + user.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		user.setActive(true);
		user.setType('G');
		user.setPassword("asgtfghg");

		if (user != null) {
			userDAO.edit(user);
			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("Situação: " + user.isActive());
			System.out.println("Tipo: " + user.getType());
			System.out.println("Nome: " + user.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}
}