package br.com.fabiano.oficina.dao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.Client;
import br.com.fabiano.oficina.domain.Employee;
import br.com.fabiano.oficina.domain.Sale;

public class SaleDAOTest {

	@Ignore
	@Test
	public void save() throws ParseException {
		ClientDAO clientDAO = new ClientDAO();
		Client client = clientDAO.search(2L);

		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = employeeDAO.search(1L);

		Sale sale = new Sale();
		sale.setAmount(new BigDecimal("25.50"));
		sale.setSchedule(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("08/10/2019 10:25:04"));
		sale.setClient(client);
		sale.setEmployee(employee);

		SaleDAO saleDAO = new SaleDAO();
		saleDAO.save(sale);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		SaleDAO saleDAO = new SaleDAO();
		List<Sale> result = saleDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (Sale sale : result) {
			System.out.println("\nValor: " + sale.getAmount());
			System.out.println("Data da Venda: " + sale.getSchedule());
			System.out.println("Cliente: " + sale.getClient().getPerson().getName());
			System.out.println("Funcionário: " + sale.getEmployee().getPerson().getName());
		}
	}

	@Ignore
	@Test
	public void search() {
		SaleDAO saleDAO = new SaleDAO();
		Sale sale = saleDAO.search(1L);

		if (sale != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nValor: " + sale.getAmount());
			System.out.println("Data da Venda: " + sale.getSchedule());
			System.out.println("Cliente: " + sale.getClient().getPerson().getName());
			System.out.println("Funcionário: " + sale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		SaleDAO saleDAO = new SaleDAO();
		Sale sale = saleDAO.search(2L);

		if (sale != null) {
			saleDAO.delete(sale);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nValor: " + sale.getAmount());
			System.out.println("Data da Venda: " + sale.getSchedule());
			System.out.println("Cliente: " + sale.getClient().getPerson().getName());
			System.out.println("Funcionário: " + sale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() throws ParseException {
		ClientDAO clientDAO = new ClientDAO();
		Client client = clientDAO.search(3L);

		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = employeeDAO.search(1L);

		SaleDAO saleDAO = new SaleDAO();
		Sale sale = saleDAO.search(4L);

		if (sale != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nValor: " + sale.getAmount());
			System.out.println("Data da Venda: " + sale.getSchedule());
			System.out.println("Cliente: " + sale.getClient().getPerson().getName());
			System.out.println("Funcionário: " + sale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		sale.setAmount(new BigDecimal("24.50"));
		sale.setSchedule(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("08/10/2019 10:30:04"));
		sale.setClient(client);
		sale.setEmployee(employee);

		if (sale != null) {
			saleDAO.edit(sale);
			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nValor: " + sale.getAmount());
			System.out.println("Data da Venda: " + sale.getSchedule());
			System.out.println("Cliente: " + sale.getClient().getPerson().getName());
			System.out.println("Funcionário: " + sale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}
}