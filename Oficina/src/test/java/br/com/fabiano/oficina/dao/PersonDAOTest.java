package br.com.fabiano.oficina.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.City;
import br.com.fabiano.oficina.domain.Person;

public class PersonDAOTest {

	@Ignore
	@Test
	public void save() {
		CityDAO cityDAO = new CityDAO();
		City city = cityDAO.search(10L);

		Person person = new Person();
		person.setName("Test");
		person.setCpf("25658745895");
		person.setRg("4523654785");
		person.setStreet("Rua Lira");
		person.setNumber("502");
		person.setNeighborhood("Penha");
		person.setZipCode("22780-170");
		person.setComplement("apto 703");
		person.setTelephone("2222-4444");
		person.setCellPhone("99999-9999");
		person.setEmail("test@gmail.com");
		person.setCity(city);

		PersonDAO personDAO = new PersonDAO();
		personDAO.save(person);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		PersonDAO personDAO = new PersonDAO();
		List<Person> result = personDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (Person person : result) {

			System.out.println("\nNome: " + person.getName());
			System.out.println("CPF: " + person.getCpf());
			System.out.println("RG: " + person.getRg());
			System.out.println("Rua: " + person.getStreet());
			System.out.println("Número: " + person.getNumber());
			System.out.println("Bairro: " + person.getNeighborhood());
			System.out.println("CEP: " + person.getZipCode());
			System.out.println("Complemento: " + person.getComplement());
			System.out.println("Telefone: " + person.getTelephone());
			System.out.println("Celular: " + person.getCellPhone());
			System.out.println("E-mail: " + person.getEmail());
			System.out.println("Cidade: " + person.getCity().getName());
			System.out.println("Estado: " + person.getCity().getState().getName());
		}
	}

	@Ignore
	@Test
	public void search() {
		PersonDAO personDAO = new PersonDAO();
		Person person = personDAO.search(1L);

		if (person != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nNome: " + person.getName());
			System.out.println("CPF: " + person.getCpf());
			System.out.println("RG: " + person.getRg());
			System.out.println("Rua: " + person.getStreet());
			System.out.println("Número: " + person.getNumber());
			System.out.println("Bairro: " + person.getNeighborhood());
			System.out.println("CEP: " + person.getZipCode());
			System.out.println("Complemento: " + person.getComplement());
			System.out.println("Telefone: " + person.getTelephone());
			System.out.println("Celular: " + person.getCellPhone());
			System.out.println("E-mail: " + person.getEmail());
			System.out.println("Cidade: " + person.getCity().getName());
			System.out.println("Estado: " + person.getCity().getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		PersonDAO personDAO = new PersonDAO();
		Person person = personDAO.search(5L);

		if (person != null) {
			personDAO.delete(person);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nNome: " + person.getName());
			System.out.println("CPF: " + person.getCpf());
			System.out.println("RG: " + person.getRg());
			System.out.println("Rua: " + person.getStreet());
			System.out.println("Número: " + person.getNumber());
			System.out.println("Bairro: " + person.getNeighborhood());
			System.out.println("CEP: " + person.getZipCode());
			System.out.println("Complemento: " + person.getComplement());
			System.out.println("Telefone: " + person.getTelephone());
			System.out.println("Celular: " + person.getCellPhone());
			System.out.println("E-mail: " + person.getEmail());
			System.out.println("Cidade: " + person.getCity().getName());
			System.out.println("Estado: " + person.getCity().getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings({ "unused" })
	@Ignore
	@Test
	public void edit() {
		PersonDAO personDAO = new PersonDAO();
		Person person = personDAO.search(4L);

		CityDAO cityDAO = new CityDAO();
		City city = cityDAO.search(10L);

		if (person != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nNome: " + person.getName());
			System.out.println("CPF: " + person.getCpf());
			System.out.println("RG: " + person.getRg());
			System.out.println("Rua: " + person.getStreet());
			System.out.println("Número: " + person.getNumber());
			System.out.println("Bairro: " + person.getNeighborhood());
			System.out.println("CEP: " + person.getZipCode());
			System.out.println("Complemento: " + person.getComplement());
			System.out.println("Telefone: " + person.getTelephone());
			System.out.println("Celular: " + person.getCellPhone());
			System.out.println("E-mail: " + person.getEmail());
			System.out.println("Cidade: " + person.getCity().getName());
			System.out.println("Estado: " + person.getCity().getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		person.setName("Luther");
		person.setCpf("00000000000");
		person.setRg("9999999999");
		person.setStreet("Rua Talzen");
		person.setNumber("2000");
		person.setNeighborhood("Freguesia");
		person.setZipCode("22450-200");
		person.setComplement("apto 1500");
		person.setTelephone("5555-3333");
		person.setCellPhone("97777-6666");
		person.setEmail("luther@gmail.com");
		person.setCity(city);

		if (person != null) {
			personDAO.edit(person);
			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nNome: " + person.getName());
			System.out.println("CPF: " + person.getCpf());
			System.out.println("RG: " + person.getRg());
			System.out.println("Rua: " + person.getStreet());
			System.out.println("Número: " + person.getNumber());
			System.out.println("Bairro: " + person.getNeighborhood());
			System.out.println("CEP: " + person.getZipCode());
			System.out.println("Complemento: " + person.getComplement());
			System.out.println("Telefone: " + person.getTelephone());
			System.out.println("Celular: " + person.getCellPhone());
			System.out.println("E-mail: " + person.getEmail());
			System.out.println("Cidade: " + person.getCity().getName());
			System.out.println("Estado: " + person.getCity().getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}
}