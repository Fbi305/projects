package br.com.fabiano.oficina.dao;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.Manufacturer;
import br.com.fabiano.oficina.domain.Product;

public class ProductDAOTest {

	@Ignore
	@Test
	public void save() {
		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		Manufacturer manufacturer = manufacturerDAO.search(8L);

		Product product = new Product();
		product.setDescription("LIMPA CONTATOS AEROSSOL 300ML 7 ORBI QUIMICA");
		product.setManufacturer(manufacturer);
		product.setPrice(new BigDecimal("15.00"));
		product.setQuantity(new Short("7"));
		
		ProductDAO productDAO = new ProductDAO();
		productDAO.save(product);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		ProductDAO productDAO = new ProductDAO();
		List<Product> result = productDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (Product product : result) {
			System.out.println("\nCódigo: " + product.getCod());
			System.out.println("Descrição: " + product.getDescription());
			System.out.println("Fabricante: " + product.getManufacturer().getDescription());
			System.out.println("Preço: " + product.getPrice());
			System.out.println("Quantidade: " + product.getQuantity());
		}
	}

	@Ignore
	@Test
	public void searh() {
		ProductDAO productDAO = new ProductDAO();
		Product product = productDAO.search(1L);

		if (product != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCódigo: " + product.getCod());
			System.out.println("\nDescrição: " + product.getDescription());
			System.out.println("Fabricante: " + product.getManufacturer().getDescription());
			System.out.println("Preço: " + product.getPrice());
			System.out.println("Quantidade: " + product.getQuantity());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		ProductDAO productDAO = new ProductDAO();
		Product product = productDAO.search(5L);

		if (product != null) {
			productDAO.delete(product);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nCódigo: " + product.getCod());
			System.out.println("\nDescrição: " + product.getDescription());
			System.out.println("Fabricante: " + product.getManufacturer().getDescription());
			System.out.println("Preço: " + product.getPrice());
			System.out.println("Quantidade: " + product.getQuantity());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() {
		ProductDAO productDAO = new ProductDAO();
		Product product = productDAO.search(6L);

		ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
		Manufacturer manufacturer = manufacturerDAO.search(8L);

		if (product != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCódigo: " + product.getCod());
			System.out.println("\nDescrição: " + product.getDescription());
			System.out.println("Fabricante: " + product.getManufacturer().getDescription());
			System.out.println("Preço: " + product.getPrice());
			System.out.println("Quantidade: " + product.getQuantity());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		product.setDescription("OLEO DESENGRIPANTE WHITE LUB AEROSSOL 300ML");
		product.setManufacturer(manufacturer);
		product.setPrice(new BigDecimal("15.0"));
		product.setQuantity(new Short("4"));

		if (product != null) {
			productDAO.edit(product);
			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nCódigo: " + product.getCod());
			System.out.println("\nDescrição: " + product.getDescription());
			System.out.println("Fabricante: " + product.getManufacturer().getDescription());
			System.out.println("Preço: " + product.getPrice());
			System.out.println("Quantidade: " + product.getQuantity());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}
}