package br.com.fabiano.oficina.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.Employee;
import br.com.fabiano.oficina.domain.Person;

public class EmployeeDAOTest {

	@Ignore
	@Test
	public void save() throws ParseException {
		PersonDAO personDAO = new PersonDAO();
		Person person = personDAO.search(2L);

		Employee employee = new Employee();
		employee.setWorkPermit("1234568");
		employee.setAdmissionDate(new SimpleDateFormat("dd/MM/yyyy").parse("06/06/2019"));
		employee.setPerson(person);

		EmployeeDAO employeeDAO = new EmployeeDAO();
		employeeDAO.save(employee);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		EmployeeDAO employeeDAO = new EmployeeDAO();
		List<Employee> result = employeeDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (Employee employee : result) {
			System.out.println("\nCarteira de Trabalho: " + employee.getWorkPermit());
			System.out.println("Data de Admissão: " + employee.getAdmissionDate());
			System.out.println("Nome: " + employee.getPerson().getName());
		}
	}

	@Ignore
	@Test
	public void search() {
		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = employeeDAO.search(1l);

		if (employee != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCarteira de Trabalho: " + employee.getWorkPermit());
			System.out.println("Data de Admissão: " + employee.getAdmissionDate());
			System.out.println("Nome: " + employee.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = employeeDAO.search(2L);

		if (employee != null) {
			employeeDAO.delete(employee);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nCarteira de Trabalho: " + employee.getWorkPermit());
			System.out.println("Data de Admissão: " + employee.getAdmissionDate());
			System.out.println("Nome: " + employee.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() throws ParseException {
		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = employeeDAO.search(1L);

		PersonDAO personDAO = new PersonDAO();
		Person person = personDAO.search(2L);

		if (employee != null) {
			System.out.println("\nCarteira de Trabalho: " + employee.getWorkPermit());
			System.out.println("Data de Admissão: " + employee.getAdmissionDate());
			System.out.println("Nome: " + employee.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		employee.setWorkPermit("32165889");
		employee.setAdmissionDate(new SimpleDateFormat("dd/MM/yyyy").parse("10/06/2019"));
		employee.setPerson(person);

		if (employee != null) {
			System.out.println("\nCarteira de Trabalho: " + employee.getWorkPermit());
			System.out.println("Data de Admissão: " + employee.getAdmissionDate());
			System.out.println("Nome: " + employee.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}
}