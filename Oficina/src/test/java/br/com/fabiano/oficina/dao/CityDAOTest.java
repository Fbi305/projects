package br.com.fabiano.oficina.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.City;
import br.com.fabiano.oficina.domain.State;

public class CityDAOTest {

	@Ignore
	@Test
	public void save() {
		StateDAO stateDAO = new StateDAO();

		State state = stateDAO.search(1L);

		City city = new City();
		city.setName("Campinas");
		city.setState(state);

		CityDAO cityDAO = new CityDAO();
		cityDAO.save(city);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		CityDAO cityDAO = new CityDAO();
		List<City> cities = cityDAO.list();

		System.out.println("\nTotal de registros encontrados: " + cities.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (City city : cities) {
			System.out.println("\nCódigo: " + city.getCod());
			System.out.println("Nome: " + city.getName());
			System.out.println("Código do Estado: " + city.getState().getCod());
			System.out.println("Sigla do Estado: " + city.getState().getInitials());
			System.out.println("Nome do Estado: " + city.getState().getName());
		}
	}

	@Ignore
	@Test
	public void search() {
		CityDAO cityDAO = new CityDAO();
		City city = cityDAO.search(1L);

		if (city != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCódigo: " + city.getCod());
			System.out.println("Nome: " + city.getName());
			System.out.println("Código do Estado: " + city.getState().getCod());
			System.out.println("Sigla do Estado: " + city.getState().getInitials());
			System.out.println("Nome do Estado: " + city.getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delet() {
		CityDAO cityDAO = new CityDAO();
		City city = cityDAO.search(2L);

		if (city != null) {
			cityDAO.delete(city);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nCódigo: " + city.getCod());
			System.out.println("Nome: " + city.getName());
			System.out.println("Código do Estado: " + city.getState().getCod());
			System.out.println("Sigla do Estado: " + city.getState().getInitials());
			System.out.println("Nome do Estado: " + city.getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() {
		StateDAO stateDAO = new StateDAO();
		State state = stateDAO.search(2L);

		CityDAO cityDAO = new CityDAO();
		City city = cityDAO.search(8L);

		if (city != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nCódigo: " + city.getCod());
			System.out.println("Nome: " + city.getName());
			System.out.println("Código do Estado: " + city.getState().getCod());
			System.out.println("Sigla do Estado: " + city.getState().getInitials());
			System.out.println("Nome do Estado: " + city.getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		city.setName("Nova Iguaçu");
		city.setState(state);

		if (city != null) {
			cityDAO.edit(city);

			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nCódigo: " + city.getCod());
			System.out.println("Nome: " + city.getName());
			System.out.println("Código do Estado: " + city.getState().getCod());
			System.out.println("Sigla do Estado: " + city.getState().getInitials());
			System.out.println("Nome do Estado: " + city.getState().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||\n");
		}
	}
}