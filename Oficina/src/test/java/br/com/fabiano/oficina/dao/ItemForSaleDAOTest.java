package br.com.fabiano.oficina.dao;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.Employee;
import br.com.fabiano.oficina.domain.ItemForSale;
import br.com.fabiano.oficina.domain.Product;

public class ItemForSaleDAOTest {

	@Ignore
	@Test
	public void save() {
		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = employeeDAO.search(1L);

		ProductDAO productDAO = new ProductDAO();
		Product product = productDAO.search(1L);

		ItemForSale itemForSale = new ItemForSale();
		itemForSale.setQuantity(new Short("1"));
		itemForSale.setPartialValue(new BigDecimal("15.50"));
		itemForSale.setEmployee(employee);
		itemForSale.setProduct(product);
		ItemForSaleDAO itemForSaleDAO = new ItemForSaleDAO();
		itemForSaleDAO.save(itemForSale);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		ItemForSaleDAO itemForSaleDAO = new ItemForSaleDAO();
		List<ItemForSale> result = itemForSaleDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTROS||||||||");

		for (ItemForSale itemForSale : result) {
			System.out.println("\nProduto: " + itemForSale.getProduct().getDescription());
			System.out.println("Quantidade: " + itemForSale.getQuantity());
			System.out.println("Valor de Venda: " + itemForSale.getPartialValue());
			System.out.println("Funcionário: " + itemForSale.getEmployee().getPerson().getName());
		}
	}

	@Ignore
	@Test
	public void search() {
		ItemForSaleDAO itemForSaleDAO = new ItemForSaleDAO();
		ItemForSale itemForSale = itemForSaleDAO.search(1L);

		if (itemForSale != null) {
			System.out.println("\nProduto: " + itemForSale.getProduct().getDescription());
			System.out.println("Quantidade: " + itemForSale.getQuantity());
			System.out.println("Valor de Venda: " + itemForSale.getPartialValue());
			System.out.println("Funcionário: " + itemForSale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		ItemForSaleDAO itemForSaleDAO = new ItemForSaleDAO();
		ItemForSale itemForSale = itemForSaleDAO.search(2L);

		if (itemForSale != null) {
			itemForSaleDAO.delete(itemForSale);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nProduto: " + itemForSale.getProduct().getDescription());
			System.out.println("Quantidade: " + itemForSale.getQuantity());
			System.out.println("Valor de Venda: " + itemForSale.getPartialValue());
			System.out.println("Funcionário: " + itemForSale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() {
		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = employeeDAO.search(1L);

		ProductDAO productDAO = new ProductDAO();
		Product product = productDAO.search(1L);

		ItemForSaleDAO itemForSaleDAO = new ItemForSaleDAO();
		ItemForSale itemForSale = itemForSaleDAO.search(1L);

		if (itemForSale != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nProduto: " + itemForSale.getProduct().getDescription());
			System.out.println("Quantidade: " + itemForSale.getQuantity());
			System.out.println("Valor de Venda: " + itemForSale.getPartialValue());
			System.out.println("Funcionário: " + itemForSale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		itemForSale.setQuantity(new Short("2"));
		itemForSale.setPartialValue(new BigDecimal("20.50"));
		itemForSale.setEmployee(employee);
		itemForSale.setProduct(product);

		if (itemForSale != null) {
			itemForSaleDAO.edit(itemForSale);
			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nProduto: " + itemForSale.getProduct().getDescription());
			System.out.println("Quantidade: " + itemForSale.getQuantity());
			System.out.println("Valor de Venda: " + itemForSale.getPartialValue());
			System.out.println("Funcionário: " + itemForSale.getEmployee().getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

	}
}