package br.com.fabiano.oficina.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.fabiano.oficina.domain.Client;
import br.com.fabiano.oficina.domain.Person;

public class ClientDAOTest {

	@Ignore
	@Test
	public void save() throws ParseException {
		PersonDAO personDAO = new PersonDAO();
		Person person = personDAO.search(3L);

		Client client = new Client();
		client.setRegistrationDate(new SimpleDateFormat("dd/MM/yyyy").parse("06/06/2019"));
		client.setReleased(true);
		client.setPerson(person);

		ClientDAO clientDAO = new ClientDAO();
		clientDAO.save(client);

		System.out.println("\n||||||||REGISTRO SALVO COM SUCESSO||||||||");
	}

	@Ignore
	@Test
	public void list() {
		ClientDAO clientDAO = new ClientDAO();
		List<Client> result = clientDAO.list();

		System.out.println("\nTotal de registros encontrados: " + result.size());
		System.out.println("\n||||||||REGISTRO||||||||");

		for (Client client : result) {
			System.out.println("\nData de Registro: " + client.getRegistrationDate());
			System.out.println("\nSituação: " + client.isReleased());
			System.out.println("\nNome: " + client.getPerson().getName());
		}
	}

	@Ignore
	@Test
	public void search() {
		ClientDAO clientDAO = new ClientDAO();
		Client client = clientDAO.search(1L);

		if (client != null) {
			System.out.println("\n||||||||REGISTROS||||||||");
			System.out.println("\nData de Registro: " + client.getRegistrationDate());
			System.out.println("\nSituação: " + client.isReleased());
			System.out.println("\nNome: " + client.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@Ignore
	@Test
	public void delete() {
		ClientDAO clientDAO = new ClientDAO();
		Client client = clientDAO.search(2L);

		if (client != null) {
			clientDAO.delete(client);
			System.out.println("\n||||||||REGISTRO REMOVIDO||||||||");
			System.out.println("\nData de Registro: " + client.getRegistrationDate());
			System.out.println("\nSituação: " + client.isReleased());
			System.out.println("\nNome: " + client.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}

	@SuppressWarnings("unused")
	@Ignore
	@Test
	public void edit() throws ParseException {
		ClientDAO clientDAO = new ClientDAO();
		Client client = clientDAO.search(3L);

		if (client != null) {
			System.out.println("\n||||||||REGISTRO||||||||");
			System.out.println("\nData de Registro: " + client.getRegistrationDate());
			System.out.println("\nSituação: " + client.isReleased());
			System.out.println("\nNome: " + client.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}

		client.setRegistrationDate(new SimpleDateFormat("dd/MM/yyyy").parse("10/06/2019"));
		client.setReleased(false);

		if (client != null) {
			clientDAO.edit(client);
			System.out.println("\n||||||||REGISTRO ALTERADO||||||||");
			System.out.println("\nData de Registro: " + client.getRegistrationDate());
			System.out.println("\nSituação: " + client.isReleased());
			System.out.println("\nNome: " + client.getPerson().getName());
		} else {
			System.out.println("\n||||||||NENHUM REGISTRO ENCONTRADO||||||||");
		}
	}
}