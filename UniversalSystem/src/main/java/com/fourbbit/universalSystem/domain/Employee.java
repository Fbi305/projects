package com.fourbbit.universalSystem.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
public class Employee extends GenericDomain {

	@Column(length = 15, nullable = false)
	private String workbook;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date admissionDate;

	@OneToOne
	@JoinColumn(nullable = false)
	private Person person;

	public String getWorkbook() {
		return workbook;
	}

	public void setWorkbook(String workbook) {
		this.workbook = workbook;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
}
