package com.fourbbit.universalSystem.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class Person extends GenericDomain {

	@Column(length = 50, nullable = false)
	private String name;

	@Column(length = 15, nullable = false)
	private String cpf;

	@Column(length = 15, nullable = false)
	private String rg;

	@Column(length = 30, nullable = false)
	private String street;

	@Column(length = 5, nullable = false)
	private Short number;

	@Column(length = 20, nullable = false)
	private String neighborhood;

	@Column(length = 15, nullable = false)
	private String zipCode;

	@Column(length = 10, nullable = false)
	private String complement;

	@Column(length = 20, nullable = false)
	private String telephone;

	@Column(length = 20, nullable = false)
	private String cellPhone;

	@Column(length = 50, nullable = false)
	private String email;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Short getNumber() {
		return number;
	}

	public void setNumber(Short number) {
		this.number = number;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}