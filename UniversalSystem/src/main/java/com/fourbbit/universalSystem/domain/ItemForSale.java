package com.fourbbit.universalSystem.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@SuppressWarnings("serial")
@Entity
public class ItemForSale extends GenericDomain {
	@Column(nullable = false)
	private Short qunatity;

	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal partialValue;

	@OneToOne
	@JoinColumn(nullable = false)
	private Product product;

	@OneToOne
	@JoinColumn(nullable = false)
	private Sale sale;

	public Short getQunatity() {
		return qunatity;
	}

	public void setQunatity(Short qunatity) {
		this.qunatity = qunatity;
	}

	public BigDecimal getPartialValue() {
		return partialValue;
	}

	public void setPartialValue(BigDecimal partialValue) {
		this.partialValue = partialValue;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}
}
