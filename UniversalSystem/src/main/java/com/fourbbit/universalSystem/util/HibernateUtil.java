package com.fourbbit.universalSystem.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static SessionFactory sessionFactory = createFactoryOfSections();

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private static SessionFactory createFactoryOfSections() {
		try {
			Configuration configuration = new Configuration().configure();

			ServiceRegistry register = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties())
					.build();

			SessionFactory factory = configuration.buildSessionFactory(register);

			return factory;
		} catch (Throwable ex) {
			System.out.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
}