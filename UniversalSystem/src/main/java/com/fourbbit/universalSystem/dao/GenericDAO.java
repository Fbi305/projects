package com.fourbbit.universalSystem.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fourbbit.universalSystem.util.HibernateUtil;

public class GenericDAO<Entity> {
	private Class<Entity> cla;

	@SuppressWarnings("unchecked")
	public GenericDAO() {
		this.cla = (Class<Entity>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public void save(Entity entity) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
		} catch (RuntimeException error) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw error;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Entity> list() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Criteria consult = session.createCriteria(cla);
			List<Entity> result = consult.list();
			return result;
		} catch (RuntimeException error) {
			throw error;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public Entity search(Long code) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Criteria consult = session.createCriteria(cla);
			consult.add(Restrictions.idEq(code));
			Entity result = (Entity) consult.uniqueResult();
			return result;
		} catch (RuntimeException error) {
			throw error;
		} finally {
			session.close();
		}
	}

	public void delete(Entity entity) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(entity);
			transaction.commit();
		} catch (RuntimeException error) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw error;
		} finally {
			session.close();
		}
	}

	public void update(Entity entity) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.update(entity);
			transaction.commit();
		} catch (RuntimeException error) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw error;
		} finally {
			session.close();
		}
	}
}
