package com.fourbbit.universalSystem.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.fourbbit.universalSystem.domain.State;

public class StateDAOTest {

	@Test
	@Ignore
	public void save() {
		State state = new State();
		state.setName("Rio de Janeiro");
		state.setInitials("RJ");

		StateDAO stateDAO = new StateDAO();
		stateDAO.save(state);
	}

	@Test
	@Ignore
	public void list() {
		StateDAO stateDAO = new StateDAO();
		List<State> result = stateDAO.list();

		System.out.println("\r\n" + "Total de registros encontrados: " + result.size() + "\n");

		for (State state : result) {
			System.out.println(state.getCode() + " - " + state.getInitials() + " - " + state.getName());
		}
	}

	@Test
	@Ignore
	public void search() {
		long code = 5l;

		StateDAO stateDAO = new StateDAO();
		State state = stateDAO.search(code);

		System.out.println("\n" + "Busca realizada com sucesso! ");

		if (state == null) {
			System.out.println("\n" + "Nenhum registro encontrado!" + "\n");
		} else {
			System.out.println("\n" + "Registro encontrado:" + "\n" + state.getCode() + " - " + state.getInitials()
					+ " - " + state.getName());
		}
	}

	@Test
	@Ignore
	public void delete() {
		long code = 3l;

		StateDAO stateDAO = new StateDAO();
		State state = stateDAO.search(code);

		if (state == null) {
			System.out.println("\n" + "Nenhum registro encontrado!" + "\n");
		} else {
			stateDAO.delete(state);
			System.out.println("\n" + "Registro removido:" + "\n" + state.getCode() + " - " + state.getInitials()
					+ " - " + state.getName());
		}
	}

	@Test
	public void update() {
		long code = 5l;

		StateDAO stateDAO = new StateDAO();
		State state = stateDAO.search(code);
		
		if (state == null) {
			System.out.println("\n" + "Nenhum registro encontrado!" + "\n");
		} else {
			
			System.out.println("\n" + "Registro:" + "\n" + state.getCode() + " - " + state.getInitials()
			+ " - " + state.getName());
			
			state.setInitials("MG");
			state.setName("Minas Gerais");
			stateDAO.update(state);
			
			System.out.println("\n" + "Registro alterado:" + "\n" + state.getCode() + " - " + state.getInitials()
					+ " - " + state.getName());
		}
	}	
}