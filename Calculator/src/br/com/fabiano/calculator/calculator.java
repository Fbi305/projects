package br.com.fabiano.calculator;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class calculator {

	private JFrame frmCalculator;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calculator window = new calculator();
					window.frmCalculator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */

	int num, ans;
	int calculation;

	public calculator() {
		initialize();
	}

	public void arithmeticOperation() {
		switch (calculation) {
		case 1: // Addition
			ans = num + Integer.parseInt(textField.getText());
			textField.setText(Integer.toString(ans));
			break;

		case 2: // Subtraction
			ans = num - Integer.parseInt(textField.getText());
			textField.setText(Integer.toString(ans));
			break;

		case 3: // Multiplication
			ans = num * Integer.parseInt(textField.getText());
			textField.setText(Integer.toString(ans));
			break;

		case 4: // Division
			ans = num / Integer.parseInt(textField.getText());
			textField.setText(Integer.toString(ans));
			break;
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculator = new JFrame();
		frmCalculator.setType(Type.UTILITY);
		frmCalculator.setTitle("calculator");
		frmCalculator.setResizable(false);
		frmCalculator.setBounds(100, 100, 312, 330);
		frmCalculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculator.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.RIGHT);
		textField.setEditable(false);
		textField.setFont(new Font("Tahoma", Font.BOLD, 23));
		textField.setBounds(10, 24, 282, 35);
		frmCalculator.getContentPane().add(textField);
		textField.setColumns(10);

		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "7");
			}
		});
		btn7.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn7.setBounds(10, 118, 63, 35);
		frmCalculator.getContentPane().add(btn7);

		JButton btnCle = new JButton("C");
		btnCle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(null);
			}
		});
		btnCle.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnCle.setBounds(156, 76, 63, 35);
		frmCalculator.getContentPane().add(btnCle);

		JButton btnSom = new JButton("+");
		btnSom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				num = Integer.parseInt(textField.getText());
				calculation = 1;
				textField.setText("");
			}
		});
		btnSom.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSom.setBounds(229, 76, 63, 35);
		frmCalculator.getContentPane().add(btnSom);

		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "8");
			}
		});
		btn8.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn8.setBounds(83, 118, 63, 35);
		frmCalculator.getContentPane().add(btn8);

		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "9");
			}
		});
		btn9.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn9.setBounds(156, 118, 63, 35);
		frmCalculator.getContentPane().add(btn9);

		JButton btnSub = new JButton("-");
		btnSub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				num = Integer.parseInt(textField.getText());
				calculation = 2;
				textField.setText("");
			}
		});
		btnSub.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSub.setBounds(229, 118, 63, 35);
		frmCalculator.getContentPane().add(btnSub);

		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "4");
			}
		});
		btn4.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn4.setBounds(10, 164, 63, 35);
		frmCalculator.getContentPane().add(btn4);

		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "5");
			}
		});
		btn5.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn5.setBounds(83, 164, 63, 35);
		frmCalculator.getContentPane().add(btn5);

		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "6");
			}
		});
		btn6.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn6.setBounds(156, 164, 63, 35);
		frmCalculator.getContentPane().add(btn6);

		JButton btnMul = new JButton("*");
		btnMul.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				num = Integer.parseInt(textField.getText());
				calculation = 3;
				textField.setText("");
			}
		});
		btnMul.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnMul.setBounds(229, 164, 63, 35);
		frmCalculator.getContentPane().add(btnMul);

		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "1");
			}
		});
		btn1.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn1.setBounds(10, 210, 63, 35);
		frmCalculator.getContentPane().add(btn1);

		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "2");
			}
		});
		btn2.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn2.setBounds(83, 210, 63, 35);
		frmCalculator.getContentPane().add(btn2);

		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "3");
			}
		});
		btn3.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn3.setBounds(156, 210, 63, 35);
		frmCalculator.getContentPane().add(btn3);

		JButton btnDiv = new JButton("/");
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				num = Integer.parseInt(textField.getText());
				calculation = 4;
				textField.setText("");
			}
		});
		btnDiv.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnDiv.setBounds(229, 210, 63, 35);
		frmCalculator.getContentPane().add(btnDiv);

		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textField.setText(textField.getText() + "0");
			}
		});
		btn0.setFont(new Font("Tahoma", Font.BOLD, 20));
		btn0.setBounds(10, 256, 63, 35);
		frmCalculator.getContentPane().add(btn0);

		JButton btnDot = new JButton(".");
		btnDot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String enterNumber = textField.getText() + btnDot.getText();
				textField.setText(enterNumber);
			}
		});
		btnDot.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnDot.setBounds(83, 256, 63, 35);
		frmCalculator.getContentPane().add(btnDot);

		JButton btnEqu = new JButton("=");
		btnEqu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				arithmeticOperation();
			}
		});
		btnEqu.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnEqu.setBounds(156, 256, 136, 35);
		frmCalculator.getContentPane().add(btnEqu);

		JButton btnBac = new JButton("<--");
		btnBac.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String backapace = null;

				if (textField.getText().length() > 0) {
					StringBuilder strB = new StringBuilder(textField.getText());
					strB.deleteCharAt(textField.getText().length() - 1);
					backapace = strB.toString();
					textField.setText(backapace);
				}
			}
		});
		btnBac.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnBac.setBounds(10, 76, 136, 35);
		frmCalculator.getContentPane().add(btnBac);
	}
}
