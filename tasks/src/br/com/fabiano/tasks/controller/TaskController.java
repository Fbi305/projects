package br.com.fabiano.tasks.controller;

import java.sql.SQLException;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fabiano.tasks.dao.TaskDao;
import br.com.fabiano.tasks.model.Task;

@Controller
public class TaskController {

	@RequestMapping("newTask")
	public String form() {
		return "task/form";
	}

	@RequestMapping("addTask")
	public String add(@Valid Task task, BindingResult result) throws SQLException {

		if (result.hasFieldErrors("description")) {
			return "task/form";
		}

		TaskDao dao = new TaskDao();
		dao.add(task);

		return "task/taskAdded";
	}

	@RequestMapping("listTasks")
	public String list(Model model) throws SQLException {
		TaskDao dao = new TaskDao();
		model.addAttribute("task", dao.list());

		return "task/list";
	}

	@RequestMapping("removeTask")
	public String remove(Task task) throws SQLException {
		TaskDao dao = new TaskDao();
		dao.remove(task);
		return "redirect:listTasks";
	}

	@RequestMapping("showTask")
	public String show(Long id, Model model) throws SQLException {
		TaskDao dao = new TaskDao();
		model.addAttribute("task", dao.searchForId(id));
		return "task/show";
	}

	@RequestMapping("changeTask")
	public String update(Task task) throws SQLException {
		TaskDao dao = new TaskDao();
		dao.update(task);
		return "redirect:listTasks";
	}

	@ResponseBody
	@RequestMapping("taskEnds")
	public void ends(Long id) throws SQLException {
		TaskDao dao = new TaskDao();
		dao.ends(id);
	}
}
