package br.com.fabiano.tasks.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.fabiano.tasks.conection.ConnectionFactory;
import br.com.fabiano.tasks.model.Task;

public class TaskDao {
	Connection connection;

	public TaskDao() throws SQLException {
		this.connection = new ConnectionFactory().getConnection();
	}

	public void add(Task task) {

		try {
			PreparedStatement stmt = this.connection
					.prepareStatement("insert into tasks (description) " + "values (?)");
			stmt.setString(1, task.getDescription());

			stmt.execute();
			stmt.close();

			System.out.println("Tarefa adicionada com sucesso!");

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Task> list() {
		try {
			List<Task> tasks = new ArrayList<Task>();
			PreparedStatement stmt = this.connection.prepareStatement("select * from tasks");
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				// criando objeto tarefa
				Task task = new Task();
				task.setId(rs.getLong("id"));
				task.setDescription(rs.getString("description"));
				task.setFinalized(rs.getBoolean("finalized"));

				if (rs.getDate("endDate") != null) {
					// montando data atraves do calendar
					Calendar endDate = Calendar.getInstance();
					endDate.setTime(rs.getDate("endDate"));

					task.setEndDate(endDate);
				}
				// adicionar objeto a lista
				tasks.add(task);
			}
			rs.close();
			stmt.close();

			System.out.println("Lista gerada com sucesso!");

			return tasks;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void remove(Task task) {
		try {
			PreparedStatement stmt = this.connection.prepareStatement("delete from tasks where id = ?");

			stmt.setLong(1, task.getId());
			stmt.execute();
			stmt.close();

			System.out.println("Tarefa excluida com sucesso!");

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Task searchForId(Long id) {

		try {
			PreparedStatement stmt = this.connection.prepareStatement("select * from tasks");
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				// System.out.println(rs.getLong("id")+ " // " + id);
				if (id == rs.getLong("id")) {
					// criando objeto tarefa
					Task task = new Task();
					task.setId(rs.getLong("id"));
					task.setDescription(rs.getString("description"));
					task.setFinalized(rs.getBoolean("finalized"));

					if (rs.getDate("endDate") != null) {
						// montando data atraves do calendar
						Calendar endDate = Calendar.getInstance();
						endDate.setTime(rs.getDate("endDate"));

						task.setEndDate(endDate);
					}
					System.out.println("retornada tarefa:" + task.getId());
					return task;
				}
			}
			return null;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void update(Task task) {

		try {
			PreparedStatement stmt = this.connection
					.prepareStatement("update tasks set description=?, finalized=?, endDate=? where id=?");

			stmt.setString(1, task.getDescription());
			stmt.setBoolean(2, task.isFinalized());
			if (task.getEndDate() != null) {
				stmt.setDate(3, new Date(task.getEndDate().getTimeInMillis()));
			} else {
				stmt.setDate(3, null);
			}

			stmt.setLong(4, task.getId());

			stmt.execute();
			stmt.close();

			System.out.println("Dados alterados com sucesso!");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void ends(Long id) throws SQLException {

		Task task = new TaskDao().searchForId(id);

		try {
			PreparedStatement stmt = this.connection
					.prepareStatement("update tasks set finalized=?, endDate=? where id=?");

			stmt.setBoolean(1, true);

			stmt.setDate(2, new Date(Calendar.getInstance().getTimeInMillis()));

			stmt.setLong(3, task.getId());

			stmt.execute();
			stmt.close();

			System.out.println("Dados alterados com sucesso!");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
