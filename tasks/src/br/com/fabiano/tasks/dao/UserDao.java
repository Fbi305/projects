package br.com.fabiano.tasks.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.fabiano.tasks.conection.ConnectionFactory;
import br.com.fabiano.tasks.model.User;

public class UserDao {
	
	Connection connection;
	
	public UserDao() throws SQLException{
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public boolean existeUsuario(User user){
		
		try{
		PreparedStatement stmt = this.connection.prepareStatement("select * from user where user= ?  and password= ?");
		
		stmt.setString(1, user.getUser());
		stmt.setString(2, user.getPassword());
		
		ResultSet rs = stmt.executeQuery();
		
		stmt.execute();
		
		//verifica se existe retorno na consulta
		if(rs.next())
		{
			stmt.close();
			return true;
		}
		else
		{
			stmt.close();
			return false;
		}
		
	}catch(SQLException e){
		throw new RuntimeException(e);
	}
}
}
