<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulario de Contatos</title>
</head>
<body>
	<h3>Adicionar tarefas</h3>
	
	<form:errors path="task.description"/>
	<form action="addTask" method="post">
		Descrição: <br />
		<textarea name="description" rows="5" cols="100"></textarea>
		<br /> <input type="submit" value="Adicionar">

	</form>
</body>
</html>