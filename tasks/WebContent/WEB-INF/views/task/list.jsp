<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript'" src="resources/js/jquery.js"></script>
	<script type="text/javascript">
		function endNow(id) {
			$.post("taskEnds", {
				'id' : id
			}, function() {
				$("#task_"+id).html("Finalizado");
			});
		}
	</script>
<title>Lista de Contatos</title>
</head>
<body>
	<a href="newTask">Criar nova tarefa</a>

	<br />
	<br />

	<table>
		<tr>
			<th>Id</th>
			<th>Descrição</th>
			<th>Finalizado?</th>
			<th>Data de finalização</th>
			<th>Altera</th>
			<th>Remove</th>
		</tr>
		<c:forEach items="${task}" var="task">
			<tr>
				<td>${task.id}</td>
				<td>${task.description}</td>
				<c:if test="${task.finalized eq false}">
					<td id="task_${task.id}"><a href="#"
						onclick="taskEnds(${task.id})"> Finalizar </a></td>
				</c:if>
				<c:if test="${task.finalized eq true}">
					<td>Finalizado</td>
				</c:if>
				<td><fmt:formatDate value="${task.endDate.time }"
						pattern="dd/MM/yyyy" /></td>
				<td><a href="showTask?id=${task.id}">Altera</a></td>
				<td><a href="removeTask?id=${task.id}">Remover</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>