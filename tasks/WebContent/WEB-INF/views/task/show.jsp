<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="caelum" %> 
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF=8">
	<link href="css/jquery-ui.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<title>Alterando Contatos</title>
</head>
<body>
	<h3>Alterar tarefa - ${task.id}</h3>
	<form action="changeTask" method="post">

		<input type="hidden" name="id" value="${task.id}"> Descrição:<br />
		<textarea name="description" rows="5" cols="100"><%--
			--%>${task.description}<%-- 
		--%></textarea>
		<br /> Finalizado? <input type="checkbox" name="finalized"
			value="true" ${task.finalized? 'checked' : '' } /> <br /> Data
		de finalização: <br /> <input type="text" name="endDate" value="<fmt:formatDate value="${task.endDate.time}" pattern="dd/MM/yyyy" />"/>
		<br />
		
		<input type="submit" value="Alterar" />
	</form>
</body>
</html>